const fs = require('fs')
const path = require('path')
const PNG = require('pngjs').PNG
require('yargs') // eslint-disable-line
  .command('$0 <src> [output]', 'remove gradient from src', function (yargs) {
    yargs.positional('src', {
      describe: 'Source to remove gradient from. Can be an image or a directory',
      type: 'string'
    }).positional('output', {
      describe: 'Output destination',
      type: 'string'
    })
  }, main)
  .option('overwrite', {
    type: 'boolean',
    description: 'Overwrite src image instead of making a new image'
  }).argv

function main (argv) {
  const src = path.resolve(argv.src)
  if (argv.output && !fs.existsSync(argv.output)) {
    throw new TypeError('Output directory doesn\'t exist')
  }
  if (fs.lstatSync(src).isDirectory()) { // TODO: Create dir if doesn't exist
    let images = getImagesFromDir(src)
    let outDir = __dirname
    if (argv.output) {
      let output = path.resolve(argv.output)
      if (fs.lstatSync(output).isDirectory()) {
        outDir = output
      } else {
        outDir = path.dirname(output)
      }
    }
    for (let image of images) {
      let outputFile = argv.overwrite ? image : path.join(outDir, path.basename(image))
      fs.createReadStream(image)
        .pipe(new PNG())
        .on('parsed', removeGradient(outputFile))
    }
  } else {
    let output
    if (argv.output) {
      output = path.resolve(argv.output)
    } else if (argv.overwrite) {
      output = src
    } else {
      output = path.join(path.dirname(src), 'out.png')
    }

    fs.createReadStream(src)
      .pipe(new PNG())
      .on('parsed', removeGradient(output))
  }
}

function getImagesFromDir (dir) {
  let images = []
  let files = fs.readdirSync(dir)
  for (let file of files) {
    let stat = fs.statSync(path.join(dir, file))
    if (stat.isFile() && file.endsWith('.png')) {
      images.push(path.join(dir, file))
    }
  }
  return images
}

function diff (a, b) {
  let d = a - b
  return d < 0 ? d * -1 : d
}

function removeGradient (output = 'out.png') {
  return function () {
    for (var y = 0; y < this.height; y++) {
      let gradientIdx = (this.width * y) << 2
      let gradientColorR = this.data[gradientIdx]
      let gradientColorG = this.data[gradientIdx + 1]
      let gradientColorB = this.data[gradientIdx + 2]
      for (var x = 0; x < this.width; x++) {
        var idx = (this.width * y + x) << 2

        if (
          diff(this.data[idx], gradientColorR) < 10 &&
          diff(this.data[idx + 1], gradientColorG) < 10 &&
          diff(this.data[idx + 2], gradientColorB) < 10
        ) {
          this.data[idx] = 255
          this.data[idx + 1] = 255
          this.data[idx + 2] = 255
          this.data[idx + 3] = 255
        }
      }
    }

    this.pack().pipe(fs.createWriteStream(output))
  }
}
