# Remove gradient

## Usage
```
node main.js image.png
```
It generates a file with the name `out.png`

For more usage information run `node main.js --help`

Convert a whole directory by parsing in the directory instead of an image.

## How it works
It looks at all the pixels in the left side of the image with where x = 0, and removes all the pixels where they share the same x value and sets the pixel to white if the difference between them is below 10. So the gradient must be present in the left side of the photo.

## Example
Before | After
:-----:|:-----:
![Before](example/before.png) | ![After](example/after.png)
